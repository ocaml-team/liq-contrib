Index: liq-contrib-08.11/install.sh
===================================================================
--- liq-contrib-08.11.orig/install.sh	2008-11-30 17:14:21.000000000 +0100
+++ liq-contrib-08.11/install.sh	2008-11-30 17:16:41.000000000 +0100
@@ -29,7 +29,7 @@
     echo "Using provided install directory: $2"
     INST_DIR="$2"
   fi
-find | grep '.in$' | sed -e 's#\.in$##' | while read i; do
+find . -maxdepth 1 -type f | grep '.in$' | sed -e 's#\.in$##' | while read i; do
   SCRIPT=`basename $i`
   echo "* $SCRIPT"
   rm -f "$INST_DIR/$SCRIPT"
@@ -74,7 +74,7 @@
   echo "Generating scripts..."
   rm -rf dist
   mkdir dist
-  find | grep '.in$' | while read i; do
+  find  . -maxdepth 1 -type f | grep '.in$' | while read i; do
     SCRIPT=`basename $i | sed -e 's#.in$##'`
     echo "* $SCRIPT"
     cat $i | sed -e "s#@liquidsoap@#$LIQ_BIN#" | sed -e "s#@VERSION@#$VERSION#" > "dist/$SCRIPT"
